const path = require('path')

module.exports = {
  pages: {
    index: {
      entry: 'src/main.js',
      template: 'public/index.html',
      filename: 'index.html',
    },
  },

  devServer: {
    clientLogLevel: 'warning',
    hot: true,
    contentBase: 'dist',

    compress: true,
    open: true,
    overlay: {
      warnings: false,
      errors: true,
    },
    // proxy: {
    //   '^/api': {
    //     target: 'testeamais',
    //     changeOrigin: true
    //   },
    // },    
    publicPath: '/',
    quiet: true,
    watchOptions: {
      poll: false,
      ignored: /node_modules/,
    },
    disableHostCheck: true,
  },
  chainWebpack: config => {
    // process.env.NODE_ENV === 'production'
    
    //adicionado mais um alias para compatibilidade com componentes Z
    config.resolve.alias
    .set('~', path.resolve('src'))
    .set('@scss', path.resolve('src/assets/scss'))
    .set('@img', path.resolve('src/assets/images'))
  },
  // importado diretamente no main.js, não estava carregando pós login
  // css: {
  //   loaderOptions: {
  //     scss: {
  //       additionalData: '@import "@/assets/scss/app.scss";',
  //     },
  //   },
  // },
  lintOnSave: false,
  transpileDependencies: ['vuetify'],
}
