//modelo padrão para composição de novos vuex stores
import {merge} from 'lodash'

const state = {
  moduleName:null,
  editing:null,
  list:[]
}

const getters = {
  getList:state=>state.list,
  editing:state=>state.editing,
  isEditing:state=>state.editing!==null
}

const actions = {
 
  loadList({commit,state},params={}){
    const defaultParams = {
      method:'get',
      options:{},
      ...params
    }

    return new Promise(async (resolve,reject) => {
      try {
        globalThis.fetch[defaultParams.method]
          (`${process.env.VUE_APP_API_API}/${state.moduleName}/list`,{defaultParams})
        .then(({data}) => {
            const list = data.data
            if(Array.isArray(list)){
              commit('setList',list)
              resolve(list)
            }
            else{
              throw new Error("invalid data result")
            }
        }).
        catch(error=>{
          reject(error)
        })
      }
      catch(error){
        reject(error)
      } 
    })
  },

  store({state},params={}){
    const defaultParams = {
      method:'post',
      options:{},
      data:{}, //campos do formulário
      ...params
    }

    return new Promise(async (resolve,reject) => {
      try {
        globalThis.fetch[defaultParams.method]
          (`${process.env.VUE_APP_API_API}/${state.moduleName}/store`,{
            ...defaultParams.data
          })
        .then(({data})=> {
          if(data.success){
            resolve(data.data)
          }
          else
            throw new Error('erro na criação do registro')
        }).
        catch(error=>{
          reject(error)
        })
      }
      catch(error){
        reject(error)
      } 
    })
  },  

  edit({commit,state,dispatch},params={}){
    const defaultParams = {
      method:'get',
      options:{},
      ...params
    }

    return new Promise(async (resolve,reject) => {
      try {
        globalThis.fetch[defaultParams.method]
          (`${process.env.VUE_APP_API_API}/${state.moduleName}/edit/${defaultParams.id}`,{defaultParams})
        .then(({data}) => {
          if(data.success){
            commit('setEditing',data.data)
            resolve(data.data)
          }
          else
            throw new Error('erro ao carregar registro para edição')
        }).
        catch(error=>{
          dispatch('finishEditing')
          reject(error)
        })
      }
      catch(error){
        dispatch('finishEditing')
        reject(error)
      } 
    })
  },

  finishEditing({commit}){
    commit('setEditing',null)
  },

  update({state},params={}){
    const defaultParams = {
      method:'post',
      options:{},
      data:{}, //campos do formulário
      ...params
    }

    return new Promise(async (resolve,reject) => {
      try {
        globalThis.fetch[defaultParams.method]
          (`${process.env.VUE_APP_API_API}/${state.moduleName}/update`,{
            ...defaultParams.data
          })
        .then(({data})=> {
          if(data.success){
            resolve(data.data)
          }
          else
            throw new Error('erro no update do registro')
        }).
        catch(error=>{
          reject(error)
        })
      }
      catch(error){
        reject(error)
      } 
    })
  },    

  destroy({state},data){
    return new Promise(async (resolve,reject) => {
      try {
        globalThis.fetch.post
          (`${process.env.VUE_APP_API_API}/${state.moduleName}/destroy`,{
            ...data
          })
        .then(({data})=> {
          if(data.success){
            resolve(data.data)
          }
          else
            throw new Error('erro na exclusão do aluno')
        }).
        catch(error=>{
          reject(error)
        })
      }
      catch(error){
        reject(error)
      } 
    })
  },
}
  
const mutations = {
  setList (state,data) {
    state.list = data
  },
  setEditing (state,data) {
    state.editing = data
  }
}


const make = (params)=>{
  return merge({
    state,
    getters,
    actions,
    mutations,
  },params)
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
    make
}