
const state = ()=> ({
  appLoading: false,
  leftSideBar: {
    status: true,
  }
})

const persist = [
  'leftSideBar'
]

const getters = {
  getLeftSideBar: state => state.leftSideBar,
  getLeftSideBarStatus: state => state.leftSideBar.status,
  isAppLoading: state => state.appLoading === true,
}

const mutations = {
  toggleLeftSideBarStatus (state) {
    state.teste=!state.leftSideBar.status
    state.leftSideBar.status = !state.leftSideBar.status
  },
  showLeftSideBar (state) {
    state.leftSideBar.status = true
  },
  hideLeftSideBar (state) {
    state.leftSideBar.status = false
  },
  setAppLoading (state, value) {
    state.appIsLoading = value
  },
}

export default {
  state,
  getters,
  mutations,
  persist
}
