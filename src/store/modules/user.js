//consts
const USER_SESSION_NAME = 'loggedInUser'
const CHECK_TOKEN_API_URL = `${process.env.VUE_APP_API}/sanctum/csrf-cookie`;
const AUTHENTICATION_API_URL = `${process.env.VUE_APP_API_API}/authenticate`;

const  state = () => ({ 
  name:null,
  email:null
})

const persist = [
  'name',
  'email'
]

const actions = {

  async isLoggedIn({state,dispatch}){
    const hasSession = await dispatch('hasSession')
    //alterado apenas para verificar a sessão, pela persistencia do user na store
    // return (state.name != null || state.email != null) || hasSession
    return hasSession
  },

  hasSession() {
    try {
      const userData = JSON.parse(localStorage.getItem(USER_SESSION_NAME))
      return userData.name != null && userData.email != null
    }
    catch(error){
      return false
    }
  },

  async logout({commit,dispatch}){
    await dispatch('clearUserSession')
    commit('authenticate',{ name:null, email:null })
  },
  
  registerUserSession({state}){
    localStorage.setItem(USER_SESSION_NAME, JSON.stringify({
      name:state.name,
      email:state.email 
    }))
  },
  
  clearUserSession(){
    localStorage.removeItem(USER_SESSION_NAME)
  },

  login({commit,dispatch},{username,password}){
    return new Promise(async (resolve,reject) => {
      try {
        const hasCsrf  = await globalThis.fetch.get(CHECK_TOKEN_API_URL)

        globalThis.fetch.post(AUTHENTICATION_API_URL, {
          email: username,
          password 
        })
          .then(({data})=>{
            if(data?.success){
              //authentica
              commit('authenticate',{
                username:data.username,
                email:username
              })
              //estudar se o mais correto é inserir na seção aqui, ou jogar para a implementação
              dispatch('registerUserSession').then(()=>{
                resolve(data)
              })
            }
            else{
              throw new Error("invalid email or password!")
            }
          })
          .catch(error=>{
            //o throw ao invés de lançar para o último catch, está abortando
            //checar
            reject(error)
          })
      }
      catch(error){
        reject(error)
      } 
    })
  },
}
  
const mutations = {
  authenticate (state,{username,email}) {
    state.name = username
    state.email = email
  }
}

const user = {
  namespaced: true,
  state,
  actions,
  mutations,
  persist
}

export default user
