export default {
  namespaced: true,
  state: () => ({
    visible: false,
    message: null,
    stayOpen: false,
    // style:{
    //   bgColor:null,
    //   bgOpacity:null
    // }
  }),
  actions:{
    show(overlay){
      overlay.commit('setVisibility',true)
    },
    hide(context){
      context.commit('setVisibility',false)
    },
    setMessage: (overlay,value) => {
      overlay.commit('setMessage',value)
    },
    clearMessage(overlay){
      overlay.commit('setMessage',null)
    },
  },
  mutations: {
    // setStyle: (state,value={}) => {
    //   state.style = {...style,...value}
    // },
    setVisibility: (state,value) => {
      state.visible = value
    },
    setMessage: (state,value) => {
      state.message = value
    }
  }
}
