import baseStoreModule from "./baseStoreModule"

const students = baseStoreModule.make({
  namespaced: true,
  state:{
    moduleName:'students'
  }, 
})

export default students