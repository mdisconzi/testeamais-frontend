import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

import user from './modules/user'
import dashboardBehavior from './modules/dashboard-behavior'
import overlay from './modules/overlay'

//service modules
import students from './modules/students'
  
const modules = {
  user,
  dashboardBehavior,
  overlay,
  students
}

// cria um array com todas as persistencias necessárias,
// já configuradas por módulo
const persistPaths = Object.entries(modules)
  .map(module => {
    const persist = module[1]?.persist || []
    return persist.map(p=>`${module[0]}.${p}`)
  })
  .flat()


// cria o objeto de persistencia de estado  
const dataState = createPersistedState({
  key:'vuex-persistense',
  paths: persistPaths,
})


Vue.use(Vuex)


const store = new Vuex.Store({
  modules,
  plugins: [dataState]
})

export default store
