import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const dashboardPath = '/app/dashboard'
const signindPath = '/app/sessions/sign-in'
const defaultPath = '/app/modules/students' //dashboardPath (padrão, alterado para testes apenas)

const authenticate = async (to,from,next)=>{
  const isLogged = await router.app.$store.dispatch('user/isLoggedIn')
  
  if(from.meta.requireAuth !== false && !isLogged) {
    next({ name: 'signin' })
  }
  else{
    next()
  }
}

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,

  routes: [
    {
      path: '/',
      name:'root',
      component: () => import('@/views/app/Index'),
      beforeEnter:authenticate,
      redirect: defaultPath,
      children: [
        {
          path: '/app/modules',
          component: () => import('@/views/app/modules/Modules'),
          redirect: defaultPath,
          children: [
            {
              name:'students',
              path: 'students',
              component: () => import('@/views/app/modules/students/StudentPage'),
            },
          ],
        },
        {
          name:'dashboard',
          path: dashboardPath,
          component: () => import('@/views/app/modules/Dashboard'),
        },
      ],
    },
    {
      path: '/app/sessions',
      // beforeEnter:preventGetOutWithSession,
      component: () => import('@/views/app/sessions/Sessions'),
      children: [
        {
          name:'signin',
          path: 'sign-in',
          component: () => import('@/views/app/sessions/SignIn'),
          meta:{
            requireAuth:false
          },
        },
      ],
    },
    {
      name:'logout',
      path: '*/logout',
      component: () => import('@/views/app/sessions/Logout'),
    },
    {
      name:'error-404',
      path: '*',
      component: () => import('@/views/app/sessions/Error404'),
    },
  ],
})

export default router
