import Vue from 'vue'
import './webfontloader'
import './meta'

import VueMask from 'v-mask'

import colors from './themes/default'
import { Toast, toastOptions } from './toast'
import VueSweetalert2 from 'vue-sweetalert2'
import 'sweetalert2/dist/sweetalert2.min.css'

//apply all plugins
[
    VueMask,
    [Toast, toastOptions],
    [VueSweetalert2,{
      confirmButtonColor: 'red',
      cancelButtonColor: colors.info
    }]

].forEach(el=>{
    Array.isArray(el) 
    ? Vue.use(el[0],el[1]) 
    : Vue.use(el)
})




