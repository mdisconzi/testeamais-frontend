import Vue from 'vue'
import 'vuetify/dist/vuetify.min.css'
import "./z-icons/ZorroRelativePaths.css";
import Vuetify from 'vuetify'

import light from './themes/default'

//aplica as cores do tema
import zorroDefault from './themes/zorroDefault'

import ZIcons from './z-icons'

Vue.use(Vuetify)

//apenas tema light para testes, em futuras atualizações criaria-se o dark

export default new Vuetify({
  icons: { iconfont: 'md', values: ZIcons },
  theme: {
    themes: {
      light:{
        ...light,
        ...zorroDefault
      }
    }
  }
})
