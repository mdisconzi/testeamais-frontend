import Toast, { TYPE } from 'vue-toastification'
import 'vue-toastification/dist/index.css'
import '@scss/plugins/toast.css' // ajuste do paddign

const toastOptions = {
  filterBeforeCreate: (toast, toasts) => {
    if (typeof toast.content !== 'string') {
      toast.content = 'INVALID TOAST STRING'
    }
    return toasts.filter(t => t.content === toast.content).length !== 0
      ? false
      : toast
  },
  toastDefaults: { // ToastOptions object for each type of toast
    [TYPE.ERROR]: {
      position: 'top-right',
      timeout: 3000
    // closeButton: false
    },
    [TYPE.SUCCESS]: {
      position: 'top-center',
      timeout: 2000
    }
  }
}

export { Toast, toastOptions }
