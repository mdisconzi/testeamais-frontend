import axios from 'axios'

//set axios defaults
axios.defaults.headers.common['Accept'] = 'application/json'
axios.defaults.withCredentials = true;


//implementar outros wrappers como o toast, sweetalert etc (melhorias)

//objeto abstrato para permitir a substituição por outro método de consulta
//se necessário

//a princípio só os métodos get/post

const fetch = {
    get:axios.get,
    post:axios.post,
    delete:axios.delete
}

export default fetch


