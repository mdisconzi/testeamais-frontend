import overlayController from '~/mixins/overlayController'
export default {
  mixins:[overlayController],
  metaInfo: {
    title: 'Alunos'
  },
  data () {
    return {
      dialogs: {},
      tab: null
    }
  },
  computed: {
    formsContainer () {
      return this.$refs.formsContainer || null
    },
    mainForm () {
      return (this.$refs.formsContainer && this.$refs.formsContainer.forms.main) || null
    },
    _tableListeners () {
      return {
        'on-delete': this.onDelete,
        'on-edit': this.onEdit,
        'on-create-new': this.onCreateNew,
        'on-refresh': this.onRefresh
      }
    },
    _formsContainerListeners () {
      return {
        'store-success': this.onStoreSuccess,
        'store-fail': this.onStoreFail,
        'update-success': this.onUpdateSuccess,
        'update-fail': this.onUpdateFail,
        'delete-success': this.onDeleteSuccess,
        'delete-fail': this.onDeleteFail
      }
    },
    // _dialogListeners () {
    //   return {
    //     'on-open': this.onOpen,
    //     'close-click': this.onClose,
    //     'save-click': this.saveClick,
    //     'update-click': this.updateClick
    //   }
    // }
  },
  created () {
  },
  methods: {
    onRefresh () {
      // if (this.module) {
      //   this.$store
      //     .dispatch(`${this.module}/load`)
      // }
    },
    onDelete () {
    },
    onEdit () {
    },

    onCreateNew () {
      if (this.formsContainer) {
        this.formsContainer.new()
        this.formsContainer.toggleTab()
      }
    },
    onStoreSuccess () {
    },
    onUpdateSuccess () {
    },
    onDeleteSuccess () {
    },
    onStoreFail (e) {
      this.$toast.error(`ocorreram erros, o registro não pode ser salvo: ${e}`)
    },
    onUpdateFail (e) {
      this.$toast.error(`ocorreram erros, o registro não pode ser atualizado: ${e}`)
    },
    onDeleteFail (e) {
      this.$toast.error(`ocorreram erros, o registro não pode ser excluído: ${e}`)
    }
  }
}
