import BaseComponent from './BaseComponent'
// import * as colors from '~/helpers/z-colors'
import ZTableComposer from '~/mixins/composers/ZTableComposer'


export default {
  mixins: [BaseComponent,ZTableComposer],
  // components: { MbActionsButtons },
  data () {
    return {
      rows: [],
      hidden: [],
      filters: {}
    }
  },
  computed: {
    colors () {
      return colors
    },
    table () {
      return this.$refs.ztable || this.$children[0]
    },
    isGridMode () {
      return this.table !== undefined ? this.table.isGridMode : false
    },
  },

  watch: {
    list: function (n, o) {
      this.rows = n
    }
  },
  methods: {
    toggleSubRow (item) {
      this.table.toggleSubRow(item)
    },
    toggleViewMode () {
    },
    filter () {
      this.table.filter()
    }
  }
}
