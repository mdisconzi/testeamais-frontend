import { mapActions } from 'vuex'

export default {
  methods: {
    ...mapActions({
        overlayShow:'overlay/show',
        overlayHide:'overlay/hide',
      }),
    }
}
