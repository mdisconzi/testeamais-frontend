  import { mapState, mapGetters, mapMutations } from 'vuex'

  // pode ser componentizado, mas falta tempo
  export default {
    computed: {
      ...mapState({
        leftSideBar: (state) => state.dashboardBehavior.leftSideBar,
      }),
      ...mapGetters([
        'isAppLoading',
        'getLeftSideBarStatus',
      ]),
    },
    mounted () {
    },
    methods: {
      ...mapMutations([
        'toggleLeftSideBarStatus',
      ]),
    },
  }
