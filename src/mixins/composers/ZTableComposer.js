//testes de composição de elementos

const data = () => {
  return {
  }
}

const methods = {
    tableOnEditAction(v){
      this.$emit('table-on-edit-action', v)
    },
    tableOnDestroyAction(v){
      this.$emit('table-on-destroy-action', v)
    },
    tableOnNew(v){
      this.$emit('table-on-new', v)
    },
    tableOnRefresh(v){
      this.$emit('table-on-refresh', v)
    }
}

const computed = {
  tableListeners(){
    return {
      'table-on-refresh':this.tableOnRefresh,
      'table-on-new':this.tableOnNew
    }
  },
  itemListeners(){
    return {
      'table-on-edit-action':this.tableOnEditAction,
      'table-on-destroy-action':this.tableOnDestroyAction,
    }
  },
  allTableListeners(){
    return {
      ...this.tableListeners,
      ...this.itemListeners
    }
  }
}

export default {
  data,
  computed,
  methods
}