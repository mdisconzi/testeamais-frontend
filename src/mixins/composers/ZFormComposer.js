//testes de composição de elementos

const data = () => {
  return {
    forms:{}
  }
}

const methods = {
    formValidateSuccess(v){
      this.$emit('form-validate-success', v)  
    },
    formValidateFail(v){
      this.$emit('form-validate-fail', v)  
    },
    formStoreSuccess(v){
      this.$emit('form-store-success', v)  
    },
    formStoreFail(v){
      this.$emit('form-store-fail', v)  
    },
    formUpdateSuccess(v){
      this.$emit('form-update-success', v)  
    },
    formUpdateFail(v){
      this.$emit('form-update-fail', v)  
    },
    formDestroySuccess(v){
      this.$emit('form-destroy-success', v)  
    },
    formDestroyFail(v){
      this.$emit('form-destroy-fail', v)  
    }
}

const computed = {
  formListeners(){
    return {
      'form-validate-success':this.formValidateSuccess,
      'form-validate-fail':this.formValidateFail,
      'form-store-success':this.formStoreSuccess,
      'form-store-fail':this.formStoreFail,
      'form-update-success':this.formUpdateSuccess,
      'form-update-fail':this.formUpdateFail,
      'form-destroy-success':this.formDestroySuccess,
      'form-destroy-fail':this.formDestroyFail
    }
  }
}

export default {
  data,
  computed,
  methods
}