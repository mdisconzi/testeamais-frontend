//testes de composição de elementos

const data = () => {
  return {
    dialogs:{}
  }
}

const methods = {
  dialogOnOpen () {
  },
  dialogOnClose () {
  },
  dialogCloseClick(dialog){
    dialog.close()
  }
}

const computed = {
  //se todos os eventos e métodos forem padronizados, dá pra criar os
  //listeners de forma automática, dialog-on-open:this[snakeCase(dialog-on-open)]
  dialogListeners(){
    return {
      'dialog-on-open':this.dialogOnOpen,
      'dialog-on-close':this.dialogOnClose,
      'dialog-close-click':this.dialogCloseClick
    }
  }
}

export default {
  data,
  computed,
  methods
}
