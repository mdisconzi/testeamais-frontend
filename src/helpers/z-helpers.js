import dayjs from 'dayjs'
// import ZException from '~/classes/ZException'
import { get, find } from 'lodash'
import axios from 'axios'

const dateTimeFormat = (date = null, params = {}) => {
  if ([true, null, ''].includes(date)) return null

  let formattedDate = null
  const numeric = ['year', 'month', 'day', 'hour', 'minute', 'second']

  // default for params
  const _date = params.date !== undefined ? params.date : true
  const _time = params.time !== undefined ? params.date : true

  // merge default param values with method argument params
  const options = Object.assign(
    {
      year: _date,
      month: _date,
      day: _date,
      hour: _time,
      minute: _time,
      second: _time,
      locale: 'en-US',
      hour12: false,
      timeZone: 'UTC' // "America/Los_Angeles"
    },
    params
  )

  // set or remove attrs from options
  numeric.forEach(el => {
    if (options[el]) options[el] = 'numeric'
    else delete options[el]
  })

  try {
    const d = new Date(date)
    formattedDate = new Intl.DateTimeFormat(
      options.locale,
      options
    ).format(d)
  } catch (err) {
    formattedDate = 'invalid date'
  }
  return formattedDate
}

const dateHumanized = (date = '', now) => {
  if ([null, undefined].includes(now)) { now = dayjs() }
  const _dt = typeof date === 'string' ? dayjs(date) : date
  return _dt.isBefore(now) ? now.to(_dt) : now.from(_dt)
}

const getFirstInstance = (root = null, type = null, prop = null, loops = 20) => {
  let ret = null
  do {
    try {
      if (root && (root[prop] instanceof type)) {
        ret = root[prop]
        break
      }
      root = root.$parent
    } catch (err) {
      ret = null
      break
    }
  } while (--loops && root.$parent)

  return ret
}

const isEquals = (a, b) => {
  if (typeof a !== 'string' || typeof b !== 'string') return false
  return a.toLowerCase() === b.toLowerCase()
}

const tabToggle = (_this, ref) => {
  (Array.isArray(ref) ? ref : [ref]).forEach(el => {
    if (!_this.$refs[el].isActive) { _this.$refs[el].toggle() }
  })
}

const manageErrorBag = (errors = {}, fields = {}, throwError = false) => {
  errors = (Array.isArray(errors))
    ? errors
    : errors.response &&
  errors.response.data &&
  errors.response.data.errors;

  (async () => {
    for (const index in errors || []) {
      const m = get(fields, index) ||
      find(fields, function (f) {
        return f.name === index || f.index === index
      })

      if (m) {
        const n = m.el
        if (n) {
          throwError = true
          let msg = errors[index][0]
          const alias = m.label || m.alias || m.name
          msg = msg.replace(m.name, alias)
          m.errorMessages = [msg || 'error']
          n.toggleRecursiveTabs()
          n.$toast.error(msg)
          setTimeout(() => {
            n.input.focus()
          }, 120)
          break
        }
      }
    }
  })()

  return !!throwError
}

const is422Error = (err) => {
  return ((err && err.message) || '').includes('code 422')
}

const flatten = (collection = {}, prefix = '') => {
  const map = {}
  for (const f in collection) {
    map[`${prefix}.${f}`] = collection[f]
  }
  return map
}

const isFunction = (e) => typeof e === 'function'

const getCep = (cep = null) => {
  const errorMessage = new Error('cep inválido!')

  return new Promise((resolve, reject) => {
    try {
      cep = (cep && cep.replace(/\D+/g, '')) || ''
      if (cep.length !== 8) { throw errorMessage }
      axios.get(`https://viacep.com.br/ws/${cep}/json/`)
        .then(({ data }) => {
          if (data === 'erro') throw errorMessage
          resolve(data)
        }).catch(() => {
          throw errorMessage
        })
    } catch (err) {
      reject(errorMessage)
    }
  })
}

function isValidCPF(cpf) {
  if (typeof cpf !== 'string') return false
  cpf = cpf.replace(/[^\d]+/g, '')
  if (cpf.length !== 11 || !!cpf.match(/(\d)\1{10}/)) return false
  cpf = cpf.split('')
  const validator = cpf
      .filter((digit, index, array) => index >= array.length - 2 && digit)
      .map( el => +el )
  const toValidate = pop => cpf
      .filter((digit, index, array) => index < array.length - pop && digit)
      .map(el => +el)
  const rest = (count, pop) => (toValidate(pop)
      .reduce((soma, el, i) => soma + el * (count - i), 0) * 10) % 11 % 10
  return !(rest(10,2) !== validator[0] || rest(11,1) !== validator[1])
}


const ufs = {
  ac: 'Acre',
  al: 'Alagoas',
  ap: 'Amapá',
  am: 'Amazonas',
  ba: 'Bahia',
  ce: 'Ceará',
  df: 'Distrito Federal',
  es: 'Espírito Santo',
  go: 'Goiás',
  ma: 'Maranhão',
  mt: 'Mato Grosso',
  ms: 'Mato Grosso do Sul',
  mg: 'Minas Gerais',
  pa: 'Pará',
  pb: 'Paraíba',
  pr: 'Paraná',
  pe: 'Pernambuco',
  pi: 'Piauí',
  rj: 'Rio de Janeiro',
  rn: 'Rio Grande do Norte',
  rs: 'Rio Grande do Sul',
  ro: 'Rondônia',
  rr: 'Roraima',
  sc: 'Santa Catarina',
  sp: 'São Paulo',
  se: 'Sergipe',
  to: 'Tocantins'
}

const buildFormData = (formData, data, parentKey) => {
  if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
    Object.keys(data).forEach(key => {
      buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key)
    })
  } else {
    const value = data == null ? '' : data

    formData.append(parentKey, value)
  }
}

const getFormData = (data) => {
  const formData = new FormData()
  buildFormData(formData, data)
  return formData
}

export {
  dateTimeFormat, dateHumanized, getFirstInstance, isEquals, tabToggle,
  manageErrorBag, flatten, ufs, isFunction, getCep, is422Error, getFormData,
  isValidCPF
}
