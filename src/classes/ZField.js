import ZException from '~/classes/ZException'
import { empty } from '~/helpers/z-validators'
import { isFunction } from '~/helpers/z-helpers'

export default class ZField {
  constructor (args = {}) {
    
    const id = globalThis._.uniqueId('zfid-')
    this.id = id
    this.color="zPrimary"
    this.ref = globalThis._.uniqueId('zf-')
    this.cType = 'v-text-field'
    this.el = null
    this.value = undefined
    this.errorMessages = []
    this.rules = []
    this.hint = null
    this.lowercase = false
    this.uppercase = false
    this.switch = false
    this.order = 99
    this.zTab = null
    this.dense = true
    this.editable = true
    this.disabled = false
    this.ignore = false
    this.resetIgnore = false
    this.zTabEl = undefined

    this.set = (n) => {
      this.el.model = n
    }
    this.get = () => {
      // return this.el.model
      return this.value
    }
    this.customGet = () => {
      return this.get()
    }
    this.customSet = (v) => {
      return this.set(v)
    }

    this.reset = () => {
      if (this.resetIgnore === false) {
        if (isFunction(this.defaultValue)) {
          this.set(this.defaultValue())
        } else {
          this.el.model = null
        }
        this.errorMessages = []
      }
    }

    this.setProp = (i, v) => {
      this[i] = v
    }

    this.validate = (p = {}) => {
      p = Object.assign({

      }, p)

      const r = this.rules || []
      const type = this.validateAs || this.constructor.name
      const v = !r.some(el => {
        try {
          const f = el(this.value)
          switch (type) {
            default:
              if ((this.required && f !== true) || f !== true) { throw new ZException(f) }
          }
        } catch (err) {
          if (p.single) {
            this.errorMessages = [err.message || 'error']
            if (p.tabToggle && (this.el.zTabEl && this.ignoreTab !== true)) {
              console.log('aqui deveria vir o campo com as tabs', this)
              this.el.toggleRecursiveTabs(this.el)
            }
            if (p.toast && this.ignoreToast !== true) { this.el.$toast.error(err.message) }
            if (p.focus && this.ignoreFocus !== true) {
              setTimeout(() => {
                this.el.input.focus()
              }, 120)
            }
            return true
          } else {
            this.errorMessages = [err.message || 'error']
            if (r.length === 1) {
              setTimeout(() => {
                this.el.input.focus()
              }, 120)
            }
            return true
          }
        }
        this.errorMessages = []
        return false
      })

      return {
        valid: v,
        message: !v
          ? this.errorMessages[0]
          : null,
        el: this
      }
    }

    this.created = (t) => {
      if (isFunction(this.defaultValue)) {
        this.set(this.defaultValue())
      }
    }

    this.toggleTab = () => {
      this.el.toggleRecursiveTabs(this)
    }


    this.mounted = (t) => {
      this.el = t
      if (this.required) {
        this.rules.push((v) => empty(this.get(v)) ||
         `${this.label} obrigatório`.toLowerCase())
      }
    }
    Object.assign(this, args)
  }
}
