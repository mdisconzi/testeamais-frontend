import ZField from '~/classes/ZField'
import ZSwitchBtn from '~/classes/ZSwitchBtn'
import { phone } from '~/helpers/z-validators'

export default class ZPhone extends ZField {
  constructor (args) {
    super(args)

    this.label = 'Celular'
    this.zMask = '(##)#####-####'
    this.type = 'tel'
    this.rules = [
      (v) => {
        return phone(v, `${this.label} inválido`.toLowerCase())
      }
    ]

    this.customGet = () => {
      if (this.switch) {
        return this.value
          ? {
              value: this.value,
              whatsapp: this.switch.get()
            }
          : null
      }
      return this.get()
    }
    this.set = (n) => {
      this.el.model = null
      if (n && typeof n === 'object') {
        this.el.model = n.value
        this.switch.value = n.whatsapp
      } else {
        this.el.model = n
      }
    }

    if (this.whatsapp === true || typeof this.whatsapp === 'object') {
      this.switch = new ZSwitchBtn(Object.assign({}, {
        slot: 'append-outer',
        true: {
          icon: 'whatsapp',
          color: '#3ebd4e',
          label: 'é whatsapp'
        },
        false: {
          icon: 'whatsapp',
          color: '#ccc',
          label: 'não é whatsapp'
        }
      }, this.whatsapp))
    }

    Object.assign(this, args)
  }
}
