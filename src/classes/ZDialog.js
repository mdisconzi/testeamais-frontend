const ZDialog = class ZDialog {
  constructor (args = {}) {
    
    this.bind = {
      persistent:true,
      title:"Unamed Dialog",
      eager:true,
      title:'',
      icon:'save',
      iconColor:'primary',
      closeIcon:'close',
      showIcon:true,
      noClickAnimation:true,
      ...args
    }
    
    this.methods = {
      openCallback(){},
      closeCallback(){},
      ...args?.methods||{},
      setProp: (i, v) => {
        this[i] = v
      }
    }

    //verificar mais a fundo se não gera erro em modo estrito
    this.load = (vueEl)=>{
      this.component = vueEl 
    }

    this.getComponent = ()=>{
      return this.component
    }

    this.open = function(){
      this.getComponent().open()
    }

    this.close = function(){
      this.getComponent().close()
    }

    //  Object.assign(this,args)
  }
}

export default ZDialog

