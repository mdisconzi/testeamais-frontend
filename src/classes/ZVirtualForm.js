import ZException from '~/classes/ZException'
import { forIn, set } from 'lodash'
export default class ZVirtualForm {
  constructor (args = {}) {
    this.vForm = null,
    this.fields = []
    this.toast = true
    this.single = false
    this.isEditing = false
    this.tabToggle = true
    this.focus = true
    this.autoIndex = false

    this.validate = (p = {}) => {
      const { toast, single, tabToggle, focus } = this
      return new Promise((resolve, reject) => {
        (async () => {
          const errors = []
          for (const el of Object.values(this.fields)
            .sort((a, b) => a.order - b.order)
            .filter(el => el.ignore !== true)
            .filter(el => {
              //ignora não editáveis
              return !(el.editable === false && this.isEditing)
            })) {
            const v = el.validate({
              toast,
              single,
              tabToggle,
              focus
            })
            if (!v.valid) { errors.push(el) }
          }
          if (errors.length) {
            const v = errors[0].validate({
              toast,
              single: true,
              tabToggle,
              focus
            })
            
            //força a ativação de todos os campos pelo vForm
            this.vForm?.validate()
            return reject(new ZException(v.message, v))
          }
          
          const validData = this.isEditing ? { ...p, onlyEditables: true } : p
          
          return resolve(this.getValues(validData))
        })()
      })
    }

    this.getValues = (p = {}) => {
      p = {
        undefToNull: true,
        onlyEditables: false,
        formData: false,
        ...p
      }

      const map = p.formData ? new FormData() : {}

      forIn(this.fields, (m, key) => {
        
        const editable = !(p.onlyEditables && m.editable === false && this.isEditing)

        if (m.ignore !== true && editable) {
          const vl = m.customGet()
          const i = m.index || key

          if ((p.undefToNull && vl === undefined) || vl === null) {
            p.formData
              ? map.append(i, '')
              : set(map, i, null)
          } else {
            if (p.formData) {
              if (Array.isArray(vl)) {
                vl.forEach(v => {
                  map.append(`${i}[]`, v)
                })
              } else {
                map.set(i, vl)
              }
            } else { set(map, i, vl) }
          }
        }
      })
      return map
    }

    this.fill = (data) => {
      return new Promise((resolve, reject) => {
        try {
          (async () => {
            for (const f in this.fields) {
              if (Object.prototype.hasOwnProperty.apply(data, [f])) {
                const n = this.fields[f]
                if (n.el && n.ignore !== true) {
                  const v = globalThis._.get(data, n.index || f, null)
                  // verifica se é returnObject passa o data completo para
                  // implementação
                  if (n.returnObject === true) {
                    n.customSet(v, data)
                  } else {
                    n.customSet(v)
                  }
                }
             
                if(this.isEditing){
                  if(n.editable === false){
                    console.log("deveria apagar")
                    console.log(">>>",n.editable,f)
                    // n.disabled
                    // n.el.$attrs.setProp('disabled',true)
                    // n.el.$attrs.disabled = n.el.$attrs.editable === false
                    n.disabled = true
                  }
                }
              }
            }
          })()
          resolve(true)
        } catch (err) {
          return reject(new ZException('ZVirtualForm fill error', err))
        }
      })
    }

    this.reset = () => {
      return new Promise((resolve, reject) => {
        try {
          (async () => {
            for (const f in this.fields) {
              const n = this.fields[f]
              n.reset()

              // n.el.$attrs.disabled = false
               n.disabled = false
            }
          })()
          this.vForm?.reset()
          resolve(true)
        } catch (err) {
          return reject(new ZException('ZVirtualForm fill error', err))
        }
      })
    }

    this.manageErrorBag = (errors = {}) => {
      console.log('errors, key=>value', errors)
    }

    Object.assign(this, args)
  }
}
