import Vue from 'vue'
import lodash from 'lodash'
import store from './store' //sorte to use order (store used in router)
import router from './router'
import App from './App.vue'

// default style
import './assets/scss/app.scss'
import vuetify from './plugins/vuetify'
import fetch from './plugins/wrappers/fetch'

// default plugins / Vue Use
import './plugins/bootstrap'

//monta todos os componentes a partir de um caminho
const componentContexts = [
  require.context('@/components/', true, /\.vue$/),
  require.context('@/views/app/modules/', true, /\.vue$/)
]

componentContexts.forEach(context=>{
  context.keys()
    .forEach(key =>
    Vue.component(key.split('/').pop().split('.')[0],context(key).default))
})


Object.assign(globalThis,{
  '_':lodash,
  'Vue':Vue,
  'fetch':fetch // em caso de substituição do axios por outro plugin
})


//Vue Instance
new Vue({
    store,
    router,
    vuetify,
    render: h => h(App),
  }).$mount('#app')
